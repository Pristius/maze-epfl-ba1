package ch.epfl.maze.physical;

import java.util.ArrayList;
import java.util.List;

/**
 * Daedalus in which predators hunt preys. Once a prey has been caught by a
 * predator, it will be removed from the daedalus.
 * 
 */

public final class Daedalus extends World {

	/**
	 * Constructs a Daedalus with a labyrinth structure
	 * 
	 * @param labyrinth
	 *            Structure of the labyrinth, an NxM array of tiles
	 */
	private ArrayList<Prey> preys = new ArrayList<Prey>();
	private ArrayList<Predator> predators = new ArrayList<Predator>();
	private ArrayList<Prey> preysCopy = new ArrayList<Prey>();
	private ArrayList<Predator> predatorsCopy = new ArrayList<Predator>();

	public Daedalus(int[][] labyrinth) {
		super(labyrinth);
		// TODO
	}

	@Override
	public boolean isSolved() {
		// TODO
		if(preys.size()==0){
			return true;
		}
		return false;
	}

	/**
	 * Adds a predator to the daedalus.
	 * 
	 * @param p
	 *            The predator to add
	 */

	public void addPredator(Predator p) {
		// TODO
		predators.add(p);
		predatorsCopy.add(p);
	}

	/**
	 * Adds a prey to the daedalus.
	 * 
	 * @param p
	 *            The prey to add
	 */

	public void addPrey(Prey p) {
		// TODO
		preys.add(p);
		preysCopy.add(p);
	}

	/**
	 * Removes a predator from the daedalus.
	 * 
	 * @param p
	 *            The predator to remove
	 */

	public void removePredator(Predator p) {
		// TODO
		predators.remove(p);
	}

	/**
	 * Removes a prey from the daedalus.
	 * 
	 * @param p
	 *            The prey to remove
	 */

	public void removePrey(Prey p) {
		// TODO
		preys.remove(p);
	}

	@Override
	public List<Animal> getAnimals() {
		// TODO
		ArrayList<Animal> animals = new ArrayList<Animal>();
		animals.addAll(preys);
		animals.addAll(predators);
		return animals;
	}

	/**
	 * Returns a copy of the list of all current predators in the daedalus.
	 * 
	 * @return A list of all predators in the daedalus
	 */

	public List<Predator> getPredators() {
		ArrayList<Predator> newPredators = new ArrayList<Predator>(predators);
		// TODO
		return newPredators;
	}

	/**
	 * Returns a copy of the list of all current preys in the daedalus.
	 * 
	 * @return A list of all preys in the daedalus
	 */

	public List<Prey> getPreys() {
		ArrayList<Prey> newPreys = new ArrayList<Prey>(preys);
		// TODO
		return newPreys;
	}

	/**
	 * Determines if the daedalus contains a predator.
	 * 
	 * @param p
	 *            The predator in question
	 * @return <b>true</b> if the predator belongs to the world, <b>false</b>
	 *         otherwise.
	 */

	public boolean hasPredator(Predator p) {
		// TODO
		for(Predator predator : predators){
			if(predator==p){
				return true;
			}
		}
		return false;
	}

	/**
	 * Determines if the daedalus contains a prey.
	 * 
	 * @param p
	 *            The prey in question
	 * @return <b>true</b> if the prey belongs to the world, <b>false</b>
	 *         otherwise.
	 */

	public boolean hasPrey(Prey p) {
		// TODO
		for(Prey prey : preys){
			if(prey==p){
				return true;
			}
		}
		return false;
	}

	@Override
	public void reset() {
		preys.clear();
		predators.clear();
		preys.addAll(preysCopy);
		predators.addAll(predatorsCopy);
		// TODO
	}
}
