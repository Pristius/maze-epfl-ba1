package ch.epfl.maze.physical.pacman;

import java.util.List;
import java.util.Random;

import ch.epfl.maze.physical.Animal;
import ch.epfl.maze.physical.Daedalus;
import ch.epfl.maze.physical.Predator;
import ch.epfl.maze.physical.Prey;
import ch.epfl.maze.util.Direction;
import ch.epfl.maze.util.Vector2D;

/**
 * Pink ghost from the Pac-Man game, targets 4 squares in front of its target.
 * 
 */

public class Pinky extends Predator {

	/**
	 * Constructs a Pinky with a starting position.
	 * 
	 * @param position
	 *            Starting position of Pinky in the labyrinth
	 */

	public Pinky(Vector2D position) {
		super(position);
		// TODO
	}
	
	private Direction previousChoice=null;
	private int scatterCounter = 0;
	@Override
	public Direction move(Direction[] choices, Daedalus daedalus) {
		scatterCounter++;
		Vector2D objective=null;
		Vector2D home=null;
		if(previousChoice==null){
			home = getPosition();  //sets a homew
		}
		if(scatterCounter%54<=40){
			  // define objective when chase mode
		}
		else{
			objective=home;
		}
		if(choices.length==1){
			previousChoice = choices[0];
			return choices[0];
		}
		if(choices.length==2){
			if ((previousChoice == null) || !(previousChoice.isOpposite(choices[0]))){
				previousChoice = choices[0];
				return choices[0];
			}
			else{
				previousChoice = choices[1];
				return choices[1];
			}
		}
		else{
			Direction shortestPath=null;
			double fastest = 0.0;
			for(Direction c : choices){
				if ((previousChoice == null) || !(previousChoice.isOpposite(c))){
					if((fastest==0.0)||(fastest>(getPosition().add(c.toVector()).sub(objective)).dist())){
						fastest=(getPosition().add(c.toVector()).sub(objective)).dist();
						shortestPath=c;
					}
				}
			}
			previousChoice = shortestPath;
			return shortestPath;
		}
	}

	@Override
	public Animal copy() {
		return new Pinky(getPosition());
	}
}
