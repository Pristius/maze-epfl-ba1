package ch.epfl.maze.physical.pacman;

import java.util.Random;

import ch.epfl.maze.physical.Animal;
import ch.epfl.maze.physical.Daedalus;
import ch.epfl.maze.physical.Prey;
import ch.epfl.maze.util.Direction;
import ch.epfl.maze.util.Vector2D;

/**
 * Pac-Man character, from the famous game of the same name.
 * 
 */

public class PacMan extends Prey {

	public PacMan(Vector2D position) {
		super(position);
		// TODO
	}
	
	Direction previousChoice=null;
	@Override
	public Direction move(Direction[] choices, Daedalus daedalus) {
		// TODO
		int taille =  choices.length;
		if (taille == 1){
			previousChoice = choices[0];
			return choices[0];
		
		}
		
		for (int i = 1; i > 0; i++)
		{
			Random rn = new Random();
		    int rand = rn.nextInt(taille);
			if ((previousChoice == null) || !(previousChoice.isOpposite(choices[rand]))){
				previousChoice = choices[rand];
				return choices[rand];
			}
		
		}
		
		return Direction.NONE;
	}

	@Override
	public Animal copy() {
		// TODO
		return null;
	}
}
