package ch.epfl.maze.physical.pacman;

import java.util.List;

import ch.epfl.maze.physical.Animal;
import ch.epfl.maze.physical.Daedalus;
import ch.epfl.maze.physical.Predator;
import ch.epfl.maze.physical.Prey;
import ch.epfl.maze.util.Direction;
import ch.epfl.maze.util.Vector2D;

/**
 * Red ghost from the Pac-Man game, chases directly its target.
 * 
 */

public class Blinky extends Predator {

	/**
	 * Constructs a Blinky with a starting position.
	 * 
	 * @param position
	 *            Starting position of Blinky in the labyrinth
	 */

	public Blinky(Vector2D position) {
		super(position);
		// TODO
	}
	private Direction previousChoice=null;
	private int scatterCounter = 0;
	@Override
	public Direction move(Direction[] choices, Daedalus daedalus) {
		// TODO
		Vector2D home=null;
		scatterCounter++;
		if(previousChoice==null){
			home = getPosition();
		}
		Vector2D objective=null;
		if(scatterCounter%54<=40){
			List<Prey> preys;             
			preys = daedalus.getPreys();
			for(Prey p : preys){
				objective = p.getPosition(); //if more than 1 pacman, chase the latest added
			} 
		}
		else{
			 objective=home;
		}
		if(choices.length==1){
			previousChoice = choices[0];
			return choices[0];
		}
		if(choices.length==2){
			if ((previousChoice == null) || !(previousChoice.isOpposite(choices[0]))){
				previousChoice = choices[0];
				return choices[0];
			}
			else{
				previousChoice = choices[1];
				return choices[1];
			}
		}
		else{
			Direction shortestPath=null;
			double fastest = 0.0;
			for(Direction c : choices){
				if ((previousChoice == null) || !(previousChoice.isOpposite(c))){
					if((fastest==0.0)||(fastest>(getPosition().add(c.toVector()).sub(objective)).dist())){
						fastest=(getPosition().add(c.toVector()).sub(objective)).dist();
						shortestPath=c;
					}
				}
			}
			previousChoice = shortestPath;
			return shortestPath;
		}
	}

	@Override
	public Animal copy() {
		return new Blinky(getPosition());
	}
}
