package ch.epfl.maze.physical.pacman;

import java.util.List;

import ch.epfl.maze.physical.Animal;
import ch.epfl.maze.physical.Daedalus;
import ch.epfl.maze.physical.Predator;
import ch.epfl.maze.physical.Prey;
import ch.epfl.maze.util.Direction;
import ch.epfl.maze.util.Vector2D;

/**
 * Blue ghost from the Pac-Man game, targets the result of two times the vector
 * from Blinky to its target.
 * 
 */

public class Inky extends Predator {

	/**
	 * Constructs a Inky with a starting position.
	 * 
	 * @param position
	 *            Starting position of Inky in the labyrinth
	 */

	public Inky(Vector2D position) {
		super(position);
		// TODO
	}
	private Direction previousChoice=null;
	private int scatterCounter = 0;
	@Override
	public Direction move(Direction[] choices, Daedalus daedalus) {
		scatterCounter++;
		Vector2D objective=null;
		Vector2D home=null;
		if(previousChoice==null){
			home = getPosition();
		}
		if(scatterCounter%54<=40){
			Vector2D preyPos=null;
			List<Prey> preys;
			List<Predator> predators;
			preys = daedalus.getPreys();
			predators = daedalus.getPredators();
			for(Prey p : preys){
				preyPos = p.getPosition(); //if more than 1 pacman, chase the latest added
			} 
			for(Predator pr : predators){
				if(pr instanceof Blinky){
					objective = ((preyPos.mul(2)).sub(pr.getPosition()));
				}
			}
		}
		else{
			 objective=home;
		}
			
		if(choices.length==1){
			previousChoice = choices[0];
			return choices[0];
		}
		if(choices.length==2){
			if ((previousChoice == null) || !(previousChoice.isOpposite(choices[0]))){
				previousChoice = choices[0];
				return choices[0];
			}
			else{
				previousChoice = choices[1];
				return choices[1];
			}
		}
		else{
			Direction shortestPath=null;
			double fastest = 0.0;
			for(Direction c : choices){
				if ((previousChoice == null) || !(previousChoice.isOpposite(c))){
					if((fastest==0.0)||(fastest>(getPosition().add(c.toVector()).sub(objective)).dist())){
						fastest=(getPosition().add(c.toVector()).sub(objective)).dist();
						shortestPath=c;
					}
				}
			}
			previousChoice = shortestPath;
			return shortestPath;
		}
	}

	@Override
	public Animal copy() {
		return new Inky(getPosition());
	}
}
