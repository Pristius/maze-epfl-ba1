package ch.epfl.maze.physical.zoo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import ch.epfl.maze.physical.Animal;
import ch.epfl.maze.util.Direction;
import ch.epfl.maze.util.Vector2D;

/**
 * Panda A.I. that implements Trémeaux's Algorithm.
 * 
 */
public class Panda extends Animal {

	/**
	 * Constructs a panda with a starting position.
	 * 
	 * @param position
	 *            Starting position of the panda in the labyrinth
	 */

	public Panda(Vector2D position) {
		super(position);
		// TODO
	}

	/**
	 * Moves according to <i>Trémeaux's Algorithm</i>: when the panda
	 * moves, it will mark the ground at most two times (with two different
	 * colors). It will prefer taking the least marked paths. Special cases
	 * have to be handled, especially when the panda is at an intersection.
	 */
	Direction previousChoice = null;
	ArrayList<Vector2D> firstColor = new ArrayList<Vector2D>();
	ArrayList<Vector2D> secondColor = new ArrayList<Vector2D>();
	
	@Override
	public Direction move(Direction[] choices) {

		ArrayList<Direction> stChoices = new ArrayList<Direction>();
		ArrayList<Direction> ndChoices = new ArrayList<Direction>();
		ArrayList<Direction> rdChoices = new ArrayList<Direction>();
		
		for(Direction d : choices){ //sorting 
			if(firstColor.contains(d.toVector().add(getPosition()))){
				ndChoices.add(d);
			}
			if(secondColor.contains(d.toVector().add(getPosition()))){
				rdChoices.add(d);
			}
			if(!firstColor.contains(d.toVector().add(getPosition()))){
				if(!secondColor.contains(d.toVector().add(getPosition()))){
					stChoices.add(d);
				}
			}
		}
		if(choices.length==1){ //Dead end case
			coloring(choices);
			previousChoice = choices[0];
			return choices[0];
		}
		else{ 
			if(stChoices.size()!=0){ //Unmarked Available
				Random rn = new Random();
			    int rand = rn.nextInt(stChoices.size());
				previousChoice = stChoices.get(rand);
				coloring(choices);
				return stChoices.get(rand);
			}
			if(ndChoices.size()==choices.length){ //all squares are marked once
				for(int i=0; i<ndChoices.size();i++){
					if (!(previousChoice.isOpposite(ndChoices.get(i)))){
						previousChoice = ndChoices.get(i);
						coloring(choices);
						return ndChoices.get(i);
					}
				}
			}
			if(rdChoices.size()==choices.length){ //all squares are marked twice (with alternate mouse AI)
				ArrayList<Direction> goodChoice = new ArrayList<Direction>(Arrays.asList(choices));
				for(Direction d : choices){
					if(previousChoice.isOpposite(d)){
						goodChoice.remove(d);
					}
				}
				Random rn = new Random();
			    int rand = rn.nextInt(goodChoice.size());
				previousChoice = goodChoice.get(rand);
				coloring(choices);
				return goodChoice.get(rand);
			}
			else{ //choices have to be made
				Random rn = new Random();
			    int rand = rn.nextInt(ndChoices.size());
				previousChoice = ndChoices.get(rand);
				coloring(choices);
				return ndChoices.get(rand);
			}
		}
	}

	public void coloring(Direction[] choices){ //"colors" a square depending on the case
		if(choices.length <= 2){ //hallways and dead ends
			if((choices.length==1)&&(firstColor.contains(getPosition().add(choices[0].toVector())))){ //if the only choice is already marked once
				firstColor.remove(getPosition());
				secondColor.add(getPosition());
			}
			if(firstColor.contains(getPosition())){
				firstColor.remove(getPosition());
				secondColor.add(getPosition());
			}
			if(!secondColor.contains(getPosition())){
				firstColor.add(getPosition());
			}
		}
		else{  //intersections
			if(!firstColor.contains(getPosition())){
				firstColor.add(getPosition());
			}
			int count = choices.length;
			for(Direction d : choices){ //Lengthy way to mark an intersection at the very end
				if(secondColor.contains(getPosition().add(d.toVector()))){
					count--;
				}
			}
			if(count == 1){
				firstColor.remove(getPosition());
				secondColor.add(getPosition());
			}
		}
	}
	@Override
	public Animal copy() {
		// TODO
		return new Panda(getPosition());
	}
}
