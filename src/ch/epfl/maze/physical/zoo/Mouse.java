package ch.epfl.maze.physical.zoo;

import java.util.Random;

import ch.epfl.maze.physical.Animal;
import ch.epfl.maze.util.Direction;
import ch.epfl.maze.util.Vector2D;

/**
 * Mouse A.I. that remembers only the previous choice it has made.
 *  (see Mouse 2 for alternate mouse AI)
 */

public class Mouse extends Animal {

	/**
	 * Constructs a mouse with a starting position.
	 * 
	 * @param position
	 *            Starting position of the mouse in the labyrinth
	 */

	public Mouse(Vector2D position) {
		super(position);
	}

	/**
	 * Moves according to an improved version of a <i>random walk</i> : the
	 * mouse does not directly retrace its steps.
	 */
	Direction previousChoice = null;
	@Override
	public Direction move(Direction[] choices) {	
		int taille =  choices.length;
		
		if (taille == 1){
			previousChoice = choices[0];
			return choices[0];
		}
		
		for (;;){
			Random rn = new Random();
		    int rand = rn.nextInt(taille);
			if ((previousChoice == null) || !(previousChoice.isOpposite(choices[rand]))){
				previousChoice = choices[rand];
				return choices[rand];
			}
		}
	}

	
	
	@Override
	public Animal copy() {
		// TODO
		return new Mouse(getPosition());
	}
}
