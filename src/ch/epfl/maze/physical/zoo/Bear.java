package ch.epfl.maze.physical.zoo;

import ch.epfl.maze.physical.Animal;
import ch.epfl.maze.util.Direction;
import ch.epfl.maze.util.Vector2D;

/**
 * Bear A.I. that implements the Pledge Algorithm.
 * 
 */

public class Bear extends Animal {

	/**
	 * Constructs a bear with a starting position.
	 * 
	 * @param position
	 *            Starting position of the bear in the labyrinth
	 */

	public Bear(Vector2D position) {
		super(position);
		// TODO
	}

	/**
	 * Moves according to the <i>Pledge Algorithm</i> : the bear tries to move
	 * towards a favorite direction until it hits a wall. In this case, it will
	 * turn right, put its paw on the left wall, count the number of times it
	 * turns right, and subtract to this the number of times it turns left. It
	 * will repeat the procedure when the counter comes to zero, or until it
	 * leaves the maze.
	 */
	
	

	Direction previousChoice = null;
	
	int counter = 0;
	Direction favouriteDirection = Direction.NONE;
	@Override
	public Direction move(Direction[] choices) {
		int taille =  choices.length;
		
		if (favouriteDirection == Direction.NONE){
			favouriteDirection = choices[0];
		}
		if (counter == 0){
			for (int i = 0; i < taille; i++){
				if (choices[i] == favouriteDirection){
					return favouriteDirection;
				}
				counter = 1;
				previousChoice = favouriteDirection.rotateRight();
			}			
		}				
		for (int i = 0; i < taille; i++){
			if (previousChoice.relativeDirection(choices[i]) == Direction.LEFT){
				previousChoice = previousChoice.unRelativeDirection(Direction.LEFT);
				counter--;
				return previousChoice;
			}				
		}		
		for (int i = 0; i < taille; i++){
			if (previousChoice.relativeDirection(choices[i]) == Direction.UP){
				previousChoice = previousChoice.unRelativeDirection(Direction.UP);
				return previousChoice;
			}				
		}		
		for (int i = 0; i < taille; i++){
			if (previousChoice.relativeDirection(choices[i]) == Direction.RIGHT){
				previousChoice = previousChoice.unRelativeDirection(Direction.RIGHT);
				counter++;
				return previousChoice;
			}
				
		}	
		return previousChoice = previousChoice.unRelativeDirection(Direction.DOWN);
	}

	@Override
	public Animal copy() {
		// TODO
		return new Bear(getPosition());
	}
}
