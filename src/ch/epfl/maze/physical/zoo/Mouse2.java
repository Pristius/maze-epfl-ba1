package ch.epfl.maze.physical.zoo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import ch.epfl.maze.physical.Animal;
import ch.epfl.maze.util.Direction;
import ch.epfl.maze.util.Vector2D;

/**
 *  Alternate Mouse AI that doesn't rely on infinite loop if it "fails"
 *  Not worth it in term of computing efficiency, only to show the loop wasn't 
 *  a design flaw
 */

public class Mouse2 extends Animal {

	/**
	 * Constructs a mouse with a starting position.
	 * 
	 * @param position
	 *            Starting position of the mouse in the labyrinth
	 */

	public Mouse2(Vector2D position) {
		super(position);
	}

	/**
	 * Moves according to an improved version of a <i>random walk</i> : the
	 * mouse does not directly retrace its steps.
	 */
	Direction previousChoice = null;
	@Override
	public Direction move(Direction[] choices) {
		ArrayList<Direction> goodChoice = new ArrayList<Direction>(Arrays.asList(choices));
		
		if (choices.length == 1){
			previousChoice = choices[0];
			return choices[0];
		
		}
		for(Direction d : choices){
			if(previousChoice.isOpposite(d)){
				goodChoice.remove(d);
			}
		}
		Random rn = new Random();
	    int rand = rn.nextInt(goodChoice.size());
		previousChoice = goodChoice.get(rand);
		return goodChoice.get(rand);
	}

	
	
	@Override
	public Animal copy() {
		// TODO
		return new Mouse(getPosition());
	}
}
