package ch.epfl.maze.physical.zoo;

import java.util.AbstractMap;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import ch.epfl.maze.physical.World;
import ch.epfl.maze.physical.Maze;
import ch.epfl.maze.simulation.*;
import ch.epfl.maze.physical.Animal;
import ch.epfl.maze.util.Direction;
import ch.epfl.maze.util.Vector2D;

/**
 * Space Invader A.I. that implements an algorithm of your choice.
 * <p>
 * Note that this class is considered as a <i>bonus</i>, meaning that you do not
 * have to implement it (see statement: section 6, Extensions libres).
 * <p>
 * If you consider implementing it, you will have bonus points on your grade if
 * it can exit a simply-connected maze, plus additional points if it is more
 * efficient than the animals you had to implement.
 * <p>
 * The way we measure efficiency is made by the test case {@code Competition}.
 * 
 * @see ch.epfl.maze.tests.Competition Competition 
 * 
 */

public class SpaceInvader extends Animal {

	/**
	 * Constructs a space invader with a starting position.
	 * 
	 * @param position
	 *            Starting position of the mouse in the labyrinth
	 */

	public SpaceInvader(Vector2D position) {
		super(position);
		// TODO (bonus)
	}

	/**
	 * Moves according to Flood algorithm.
	 * With his alien technology, the space invader scans the labyrinth and find the quickest path to victory 
	 * ALL OUR BASES BELONGS TO US.
	 * The space invader won't move until the maze is "flooded" and the path is computed
	 * WARNING : May break the competition as it's always the fastest possible.
	 */
	
	ArrayList<SimpleEntry<Direction, Vector2D>> mapping = new ArrayList<SimpleEntry<Direction, Vector2D>>();
	@Override
	public Direction move(Direction[] choices) {  //gets all registered directions
		boolean finished = false;
		mapping.add(new SimpleEntry<Direction, Vector2D>(Direction.NONE,getPosition()));
		while(finished != true){
			for (Direction d : choices){
				
			}
		}
		return Direction.NONE;
	}

	@Override
	public Animal copy() {
		// TODO (bonus)
		return null;
	}
}