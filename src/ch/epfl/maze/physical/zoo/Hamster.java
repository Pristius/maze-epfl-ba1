package ch.epfl.maze.physical.zoo;

import java.util.ArrayList;
import java.util.Random;

import ch.epfl.maze.physical.Animal;
import ch.epfl.maze.util.Direction;
import ch.epfl.maze.util.Vector2D;

/**
 * Hamster A.I. that remembers the previous choice it has made and the dead ends
 * it has already met.
 * 
 */

public class Hamster extends Animal {

	/**
	 * Constructs a hamster with a starting position.
	 * 
	 * @param position
	 *            Starting position of the hamster in the labyrinth
	 */

	public Hamster(Vector2D position) {
		super(position);
		// TODO
	}

	/**
	 * Moves without retracing directly its steps and by avoiding the dead-ends
	 * it learns during its journey.
	 */
	private Direction previousChoice = null;
	private ArrayList<Vector2D> deadEnds = new ArrayList<Vector2D>();
	
	@Override
	public Direction move(Direction[] choices) {
		// TODO	
		ArrayList<Direction> newChoices = new ArrayList<Direction>();
		if(choices.length==1){
			deadEnds.add(getPosition());
			previousChoice = choices[0];
			return choices[0];
		}
		if(choices.length>1){
			for(int y=0; y<choices.length;y++){
				boolean valid = true; //Each choice is innocent until proven guilty
				for(int y1=0;y1<deadEnds.size();y1++){
					if(getPosition().add(choices[y].toVector()).equals(deadEnds.get(y1))){
						valid=false;
					}
				}
				if(valid){
					newChoices.add(choices[y]);
				}
			}
			if(newChoices.size()==1){
				deadEnds.add(getPosition());
				previousChoice = newChoices.get(0);
				return newChoices.get(0);
			}
			else{
				for(int z = 0;z==0;){
					Random rn = new Random();
					int rand = rn.nextInt(newChoices.size());
					if ((previousChoice == null) || !(previousChoice.isOpposite(newChoices.get(rand)))){
						previousChoice = newChoices.get(rand);
						return newChoices.get(rand);
					}
				}
			}
		}
		return Direction.NONE;
	}

	@Override
	public Animal copy() {
		return new Hamster(getPosition());
	}
}
