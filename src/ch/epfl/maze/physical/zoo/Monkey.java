package ch.epfl.maze.physical.zoo;

import java.util.Random;

import ch.epfl.maze.physical.Animal;
import ch.epfl.maze.util.Direction;
import ch.epfl.maze.util.Vector2D;

/**
 * Monkey A.I. that puts its hand on the left wall and follows it.
 * 
 */

public class Monkey extends Animal {

	/**
	 * Constructs a monkey with a starting position.
	 * 
	 * @param position
	 *            Starting position of the monkey in the labyrinth
	 */

	public Monkey(Vector2D position) {
		super(position);
		// TODO
	}

	/**
	 * Moves according to the relative left wall that the monkey has to follow.
	 */

	
	Direction previousChoice = null;
	
	@Override
	public Direction move(Direction[] choices) {
		
		int taille =  choices.length;
		if (taille == 1){
			previousChoice = choices[0];
			return choices[0];
		}
		if(previousChoice==null){
			Random rn = new Random();
			int rand = rn.nextInt(choices.length);
			previousChoice = choices[rand];
			return choices[rand];
		}
		for (int i = 0; i < taille; i++){
			if (previousChoice.relativeDirection(choices[i]) == Direction.LEFT){
				previousChoice = previousChoice.unRelativeDirection(Direction.LEFT);
				return previousChoice;
			}
				
		}
		
		for (int i = 0; i < taille; i++){
			if (previousChoice.relativeDirection(choices[i]) == Direction.UP){
				previousChoice = previousChoice.unRelativeDirection(Direction.UP);
				return previousChoice;
			}
				
		}
		
		for (int i = 0; i < taille; i++){
			if (previousChoice.relativeDirection(choices[i]) == Direction.RIGHT){
				previousChoice = previousChoice.unRelativeDirection(Direction.RIGHT);
				return previousChoice;
			}
				
		}
		// TODO
		return Direction.NONE;
	}

	@Override
	public Animal copy() {
		// TODO
		return new Monkey(getPosition());
	}
}
