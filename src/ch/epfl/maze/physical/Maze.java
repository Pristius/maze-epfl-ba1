package ch.epfl.maze.physical;

import java.util.ArrayList;
import java.util.List;

/**
 * Maze in which an animal starts from a starting point and must find the exit.
 * Every animal added will have its position set to the starting point. The
 * animal is removed from the maze when it finds the exit.
 * 
 */

public final class Maze extends World {

	private ArrayList <Animal> animals = new ArrayList<Animal>();
	private ArrayList <Animal> animalsCopy = new ArrayList<Animal>();
	
	
	/**
	 * Constructs a Maze with a labyrinth structure.
	 * 
	 * @param labyrinth
	 *            Structure of the labyrinth, an NxM array of tiles
	 */

	public Maze(int[][] labyrinth) {
		super(labyrinth);
		
		// TODO
	}

	@Override
	public boolean isSolved() {
		
		if (getAnimals().isEmpty()){
			return true;
		}
		// TODO
		return false;
	}

	@Override
	public List<Animal> getAnimals() {
		
	  ArrayList <Animal> temp = new ArrayList <Animal>();
		
		for (int i=0; i<animals.size(); i++){
			
			if (animals.get(i)!=null){
				temp.add(animals.get(i));
			}
		}
		// TODO
		return (temp);
	}

	/**
	 * Determines if the maze contains an animal.
	 * 
	 * @param a
	 *            The animal in question
	 * @return <b>true</b> if the animal belongs to the world, <b>false</b>
	 *         otherwise.
	 */

	public boolean hasAnimal(Animal a) {
		// TODO
		if (getAnimals().isEmpty()){
		return false;
	}
		else {
			return true;
		}
	}

	/**
	 * Adds an animal to the maze.
	 * 
	 * @param a
	 *            The animal to add
	 */

	public void addAnimal(Animal a) {
		
		a.setPosition(getStart());
		animals.add(a);
		animalsCopy.add(a.copy());
	
		// TODO
	}

	/**
	 * Removes an animal from the maze.
	 * 
	 * @param a
	 *            The animal to remove
	 */

	public void removeAnimal(Animal a) {
		animals.remove(a);
		
		// TODO
	}

	@Override
	public void reset() {
		animals.clear();
		ArrayList <Animal> temp = new ArrayList <Animal> ();
		for (int i=0; i<animalsCopy.size(); i++){
		  temp.add(animalsCopy.get(i).copy());
		}
		animals = temp;
	
		
		// TODO
	}
}
